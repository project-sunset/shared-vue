# Project Sunset Shared Vue components

[Storybook documentation](https://project-sunset.gitlab.io/shared-vue)

## Installation

```js
import { createApp } from 'vue'

import notifications from './shared-vue/notifications/notify'
import api from './shared/vue/api'

import store from './store'

const app = createApp(App)
app.use(store)
const notifier = notify(store)
api.notify = notifier
app.config.globalProperties.$api = api
app.config.globalProperties.$notify = notifier
```

Use of Notifications:
```js
this.$notify({loading: true})
// and later
this.$notify({loading: false})

this.$notify({
  style: 'success',  // or 'warn' or 'error'
  text: 'Some notification',
  duration: 5000, // optional
})
```