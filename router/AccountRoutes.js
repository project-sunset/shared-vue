const Account = () => import('@/shared-vue/components/account/Account')
const AddNewDevice = () => import('@/shared-vue/components/account/AddNewDevice')

export default (options) => {
  return [
    { path: 'accounts/', component: Account, name: 'accounts:home', props: { back: options.backLocation } },
    { path: 'accounts/new-device/', component: AddNewDevice, name: 'accounts:add-new-device' },
  ]
} 