import ModelForm from "../../components/ModelForm.vue"

export default {
  title: 'CRUD/ModelForm',
  component: ModelForm,
  tags: ['autodocs']
}

export const Main = {
  args: {
    modelName: 'My Form',
    modelValue: {
      name: '',
      startDate: '',
      game: '',
      description: ''
    },
    config: {
      url: 'api/example',
      fields: {
        name: { type: 'text', label: 'Name' },
        startDate: {type: 'date', label: 'Start Date'},
        game: {type: 'select', label: 'Game', options: [{label: 'AoE II', value: 'aoeii'}, {label: 'AC origins', value: 'acorigins'}], optionName: 'label', optionValue: 'value'},
        description: {type: 'textarea', label: 'Description'}
      }
    }
  }
}