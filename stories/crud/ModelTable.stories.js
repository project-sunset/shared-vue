import ModelTable from "../../components/ModelTable.vue"

export default {
  title: 'CRUD/ModelTable',
  component: ModelTable,
  tags: ['autodocs']
}

export const Main = {
  args: {
    modelName: 'Test Objects',
    modelValue: {
      name: '',
      value: 0,
    },
    data: {data: [{id: 1, name: 'test', value: 123}, {id: 2, name: 'test 2', value: 234}]},
    config: {
      url: 'api/example',
      fields: {
        name: {type: 'text', label: 'Name'},
        value: {type: 'number', label: "Value", format: 'currency'},
      }
    }
  }
}