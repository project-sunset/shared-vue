import Pagination from "../components/Pagination.vue"

export default {
  title: 'Components/Pagination',
  component: Pagination,
  tags: ['autodocs']
}

export const Main = {
  args: {
    numberOfPages: 10,
    currentPage: 1
  }
}