import Notifications from "../components/Notifications.vue"

export default {
  title: 'Components/Notifications',
  component: Notifications,
  tags: ['autodocs']
}

export const Main = {
  render() {
    return {
      template: `<button @click="$store.commit('notify/popup', {style: 'success', text: 'This is a test'})">Show</button><Notifications />`
    }
  }
}