import SelectInput from "../../components/inputs/SelectInput.vue"

export default {
  title: 'Inputs/SelectInput',
  component: SelectInput,
  tags: ['autodocs']
}

export const Main = {
  args: {
    options: [
      { label: 'AoE II', value: 'aoeii' },
      { label: 'AC origins', value: 'acorigins' }
    ],
    optionName: 'label',
    optionValue: 'value'
  }
}