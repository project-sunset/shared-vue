import NumberInput from "../../components/inputs/NumberInput.vue"

export default {
  title: 'Inputs/NumberInput',
  component: NumberInput,
  tags: ['autodocs']
}

export const Main = {}