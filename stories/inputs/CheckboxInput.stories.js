import CheckboxInput from "../../components/inputs/CheckboxInput.vue"

export default {
  title: 'Inputs/CheckboxInput',
  component: CheckboxInput,
  tags: ['autodocs']
}

export const Main = {
  args: {
    label: "Yay or nay?",
    error: ["Watch out, pirates!"]
  }
}