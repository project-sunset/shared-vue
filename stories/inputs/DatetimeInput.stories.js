import DatetimeInput from "../../components/inputs/DatetimeInput.vue"

export default {
  title: 'Inputs/DatetimeInput',
  component: DatetimeInput,
  tags: ['autodocs']
}

export const Main = {}