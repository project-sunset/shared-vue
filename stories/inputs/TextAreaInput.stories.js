import TextAreaInput from "../../components/inputs/TextAreaInput.vue"

export default {
  title: 'Inputs/TextAreaInput',
  component: TextAreaInput,
  tags: ['autodocs']
}

export const Main = {}