import DateInput from "../../components/inputs/DateInput.vue"

export default {
  title: 'Inputs/DateInput',
  component: DateInput,
  tags: ['autodocs']
}

export const Main = {}