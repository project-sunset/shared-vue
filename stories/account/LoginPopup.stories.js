import LoginPopup from "../../components/account/LoginPopup.vue"

export default {
  title: 'Account/LoginPopup',
  component: LoginPopup,
  tags: ['autodocs']
}

export const Main = {
  decorators: [() => ({template: '<div style="margin: 20em"><story /></div>'})]
}