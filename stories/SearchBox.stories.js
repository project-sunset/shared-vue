import SearchBox from "../components/SearchBox.vue"

export default {
  title: 'Components/SearchBox',
  component: SearchBox,
  tags: ['autodocs']
}

export const Main = {
  args: {
    label: 'Search for something'
  }
}

export const Loading = {
  args: {
    label: 'Search for something',
    isSearching: true
  }
}