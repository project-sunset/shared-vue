import ButtonWithConfirm from "../components/ButtonWithConfirm.vue"

export default {
  title: 'Components/ButtonWithConfirm',
  component: ButtonWithConfirm,
  tags: ['autodocs'],
}

export const Main = {
  args: {
    buttonText: 'Delete',
    confirmText: 'Are you sure?',
    confirmButtonText: 'Yes!',
    cancelButtonText: 'No!',
    buttonClass: 'button'
  },
  decorators: [() => ({template: '<div style="margin: 6em"><story /></div>'})]
}