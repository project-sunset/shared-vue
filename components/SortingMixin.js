import SortingTh from './SortingTh'

export default {
  components: {
    SortingTh
  },
  data() {
    return {
      data: [],
      sorting: {}
    }
  },
  watch: {
    sorting: {
      handler() {
        for (let key in this.sorting) {
          if (this.sorting[key] !== null) {
            this.data.sort((a, b) => {
              if (this.sorting[key] === 'asc') {
                if (typeof a[key] === 'number') {
                  return a[key] - b[key]
                } else {
                  return a[key].localeCompare(b[key])
                }
              } else {
                if (typeof a[key] === 'number') {
                  return b[key] - a[key]
                } else {
                  return b[key].localeCompare(a[key])
                }
              }
            })
          }
        }
      },
      deep: true
    }
  }
}
