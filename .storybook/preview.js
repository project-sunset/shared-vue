import { setup } from '@storybook/vue3';
import { createStore } from 'vuex';
import notificationStore from '../notifications/store'
import Auth from '../authStore'

setup((app) => {
  app.use(createStore({
    modules: {
      notify: notificationStore,
      auth: Auth,
    }
  }))
  app.config.globalProperties.$filters = {
    currency(value, na) {
      if (na && !value) return 'NA'
      if (!value) value = 0
      value = Number(value)
      return value.toLocaleString('nl-NL', { style: 'currency', currency: 'EUR' })
    }
  }
})

/** @type { import('@storybook/vue3').Preview } */
const preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
  },
};

export default preview;
